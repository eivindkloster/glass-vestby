from flask import Flask, request, abort, jsonify
from bson.objectid import ObjectId
import json
import pymongo
import datetime
import hashlib
import os
import string
import random
import pprint

# temp
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

user_must_contain = ["username", "email", "password"]
login_must_contain = ["email", "password"]
document_must_contain = ["title"]
edit_doc_content_must_contain = ["content"]

from pymongo import MongoClient
client = MongoClient()

database = client.vbdev

usersdb = database.users
tokensdb = database.tokens
documentsdb = database.documents

def random_dev_only(length):
    chars = string.ascii_letters
    rand = "".join(random.choice(chars) for i in range(length))
    print(rand)
    return rand


def testisempty(test, obj, can_contain=None):
    if can_contain == None:
        can_contain = test

    for x in test:
        #print(obj[x])
        if x not in obj:
            return True
        if obj[x] == "":
            return True
    for key in obj:
        print(key)
        if key not in can_contain:
            print("key is not allowed")
            del obj[key]
        #print(val)
    return False


def login(email, password):
    # check credentials
    user = usersdb.find_one({'email': email})

    # prob not constant time
    calc_hash = hashlib.pbkdf2_hmac("sha256", bytes(password, "utf8"), user["salt"], 150000)

    if not user["pwhash"] == calc_hash:
        print("not sucess")
        return False
    
    return create_token(user["_id"])


def create_user(user):
    # do checks
    user["created_on"] = datetime.datetime.utcnow().isoformat()
    user["ver"] = 0
    bar = bytes(user["password"], "utf8")
    salt = os.urandom(24)
    user["salt"] = salt
    user["pwhash"] = hashlib.pbkdf2_hmac("sha256", bytes(user["password"], "utf8"), salt, 150000) 
    user["discriminator"] = random.randint(1000,9999)
    del user["password"]
    new_id = usersdb.insert_one(user).inserted_id
    token = create_token(new_id)
    return new_id, user["created_on"], token, user["discriminator"]

def create_token(user_id):
    #token_tk = hex(int(os.urandom(48)))
    token_tk = random_dev_only(48)
    token = dict(_id=token_tk, user_id=user_id)
    token["created_on"] = datetime.datetime.utcnow().isoformat()
    new_id = tokensdb.insert_one(token).inserted_id
    return token_tk

def getUserDict(token):
    print(token)
    token_dict = tokensdb.find_one(dict(_id=token))
    pprint.pprint(token_dict)
    print(token_dict["user_id"])


    user = usersdb.find_one({"_id": token_dict["user_id"]})
    pprint.pprint(user)
    return user

def isValidToken():
    pprint.pprint(request.headers["authorization"])
    usr_dict = getUserDict(request.headers["authorization"])

    # If it could not find anything (as is indicated by returning None), token is invalid
    if usr_dict == None:
        abort(400)
    return usr_dict

def get_user_documents(user_id):
    docs = []
    for document in documentsdb.find({"author_id": user_id}):
        pprint.pprint(document)
        convert_obj_id(document)
        docs.append(document)
    return docs


def convert_obj_id(dict):
    for key, val in dict.items():
        if type(val) is ObjectId:
            print("Key: " + key)
            pprint.pprint(val)
            dict[key] = str(val)

        if key == "_id":
            dict["id"] = val
            del dict[key]




# -- Test routes --

@app.route('/')
def hello_world():
    #return 'Hello, World!'
    return jsonify({"test": "coolj"}), 200

@app.route('/api/users', methods=['GET', 'POST'])
def users():
    if request.method == 'POST':
        js = request.get_json()
        if testisempty(user_must_contain, js):
            abort(400)
        return 'very good'
    else:
        return jsonify({"username": "lil jetmotor", "id": "16288437956878", "discriminator": "1497"})

#@app.route('/app/')

@app.route('/api/users/<id>')
def user(id):
    return jsonify({"username": "lil jetmotor", "id": id, "discriminator": "1497"})

# -- Auth routes --

@app.route("/api/sign-up", methods=["POST"])
def sign_up():
    js = request.get_json()
    if testisempty(user_must_contain, js):
        abort(400)
    new_id, created, token, dis = create_user(js)
    return jsonify({"username": js["username"], "email": js["email"], "id": str(new_id), "created_on": created, "token": token, "discriminator": dis}), 201

@app.route("/api/login", methods=["POST"])
def log_in():
    js = request.get_json()
    if testisempty(login_must_contain, js):
        abort(400)
    token = login(js["email"], js["password"])
    if not token:
        print("not token")
        abort(400)
    return jsonify(dict(token=token)), 200


# -- Documents routes --

@app.route("/api/documents", methods=["GET"])
def r_get_documents():
    user = isValidToken()
    print("1")
    pprint.pprint(user)
    docs = get_user_documents(user["_id"])
    return jsonify(docs), 200

@app.route("/api/documents", methods=["POST"])
def r_new_document():
    user = isValidToken()
    js = request.get_json()
    if testisempty(document_must_contain, js):
        abort(400)

    # default values
    js["author_id"] = user["_id"]
    js["created"] = datetime.datetime.utcnow().isoformat()
    js["edited"] = datetime.datetime.utcnow().isoformat()
    js["sharing"] = 0
    js["can_edit"] = list()
    js["can_read"] = list()
    js["edited_by_id"] = user["_id"]
    js["content"] = ""

    new_id = documentsdb.insert_one(js).inserted_id

    # temporary 
    # js["id"] = str(new_id)
    # del js["_id"]

    convert_obj_id(js)

    return jsonify(js), 201

@app.route("/api/documents/<id>", methods=["GET"])
def r_get_doc(id):
    user = isValidToken()
    document = documentsdb.find_one({"_id": ObjectId(id), "author_id": user["_id"]})
    if document is None:
        abort(400)
    
    convert_obj_id(document)

    return jsonify(document), 200

@app.route("/api/documents/<id>", methods=["PATCH"])
def r_patch_doc(id):
    user = isValidToken()

    js = request.get_json()
    if testisempty(edit_doc_content_must_contain, js):
        abort(400)

    document = documentsdb.find_one({"_id": ObjectId(id), "author_id": user["_id"]})
    if document is None:
        abort(400)

    document["content"] = js["content"]

    #documentsdb.insert_one(document) 
    documentsdb.replace_one({"_id": ObjectId(id)}, document)
    
    convert_obj_id(document)

    return jsonify(document), 200








# -- Error handlers --   

@app.errorhandler(400)
def malformed(error):
    return jsonify({"error": "Malformed request", "code": "400"}), 400

@app.errorhandler(500)
def internal(error):
    return jsonify({"error": "Generic error", "code": "400"}), 400